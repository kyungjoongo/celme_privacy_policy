<h1>Personal Face Image</h1>

The CelMe app uses personal face images to find celebrities who look alike.

Personal face images are never shared with third parties and are stored on a file server and deleted over time.

If you have any questions about this Privacy Policy, please contact us.

<b>Email: jessicalabs77777@gmail.com</b>

